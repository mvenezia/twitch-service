package main

import "gitlab.com/mvenezia/twitch-service/cmd/twitch-service/cmd"

func main() {
	cmd.Execute()
}
