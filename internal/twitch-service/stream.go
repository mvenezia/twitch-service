package twitchservice

import (
	"encoding/json"
	pb "gitlab.com/mvenezia/twitch-service/pkg/generated/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"io/ioutil"
	"net/http"
	url2 "net/url"
	"time"

	"github.com/Pallinder/sillyname-go"
)

type LiveResponse struct {
	Live bool `json:live`
	Popularity float32 `json:popularity`
}

const (
	prefixTracerState  = "x-b3-"
	zipkinTraceID      = prefixTracerState + "traceid"
	zipkinSpanID       = prefixTracerState + "spanid"
	zipkinParentSpanID = prefixTracerState + "parentspanid"
	zipkinSampled      = prefixTracerState + "sampled"
	zipkinFlags        = prefixTracerState + "flags"
	requestID          = "x-request-id"
	otSpanContext      = "x-ot-span-context"
)

var otHeaders = []string{
	zipkinTraceID,
	zipkinSpanID,
	zipkinParentSpanID,
	zipkinSampled,
	zipkinFlags,
	requestID,
	otSpanContext,
}

func injectHeadersIntoMetadata(ctx context.Context, req *http.Request) {
	headers, _ := metadata.FromIncomingContext(ctx)
	for _, h := range otHeaders {
		if v := headers.Get(h); len(v) > 0 && len(v[0]) > 0 {
			logger.Infof("Setting %s to %s\n", h, v[0])
			req.Header.Set(h, v[0])
		}
	}
}


func (s *Server) GetStreamInformation(ctx context.Context, in *pb.GetStreamMsg) (*pb.GetStreamReply, error) {
	SetLogger()
	name := sillyname.GenerateStupidName()
	url := "http://stream-live-service/api/v1/live.php?id=" + url2.QueryEscape(in.StreamId)

	liveClient := http.Client{Timeout: time.Second * 2}
	liveRequest, err := http.NewRequest(http.MethodGet, url, nil)
	injectHeadersIntoMetadata(ctx, liveRequest)
	liveResult, err := liveClient.Do(liveRequest)
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	liveBody, err := ioutil.ReadAll(liveResult.Body)
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	liveResponse := LiveResponse{}
	err = json.Unmarshal(liveBody, &liveResponse)
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}

	return &pb.GetStreamReply{Name: name, Live: liveResponse.Live}, nil
}
