package twitchservice

import (
	"github.com/juju/loggo"
	"gitlab.com/mvenezia/twitch-service/pkg/util/log"
)

var (
	logger loggo.Logger
)

type Server struct{}

func SetLogger() {
	logger = log.GetModuleLogger("internal.twitch-service", loggo.INFO)
}
