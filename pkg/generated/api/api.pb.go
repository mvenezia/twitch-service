// Code generated by protoc-gen-go. DO NOT EDIT.
// source: api.proto

package twitchservice

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options"
import _ "google.golang.org/genproto/googleapis/api/annotations"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type GetStreamMsg struct {
	// Stream id
	StreamId             string   `protobuf:"bytes,1,opt,name=stream_id,json=streamId,proto3" json:"stream_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetStreamMsg) Reset()         { *m = GetStreamMsg{} }
func (m *GetStreamMsg) String() string { return proto.CompactTextString(m) }
func (*GetStreamMsg) ProtoMessage()    {}
func (*GetStreamMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_00212fb1f9d3bf1c, []int{0}
}
func (m *GetStreamMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetStreamMsg.Unmarshal(m, b)
}
func (m *GetStreamMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetStreamMsg.Marshal(b, m, deterministic)
}
func (dst *GetStreamMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetStreamMsg.Merge(dst, src)
}
func (m *GetStreamMsg) XXX_Size() int {
	return xxx_messageInfo_GetStreamMsg.Size(m)
}
func (m *GetStreamMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_GetStreamMsg.DiscardUnknown(m)
}

var xxx_messageInfo_GetStreamMsg proto.InternalMessageInfo

func (m *GetStreamMsg) GetStreamId() string {
	if m != nil {
		return m.StreamId
	}
	return ""
}

type GetStreamReply struct {
	// What is the stream's name
	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	// Is the stream live now?
	Live                 bool     `protobuf:"varint,2,opt,name=live,proto3" json:"live,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetStreamReply) Reset()         { *m = GetStreamReply{} }
func (m *GetStreamReply) String() string { return proto.CompactTextString(m) }
func (*GetStreamReply) ProtoMessage()    {}
func (*GetStreamReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_00212fb1f9d3bf1c, []int{1}
}
func (m *GetStreamReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetStreamReply.Unmarshal(m, b)
}
func (m *GetStreamReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetStreamReply.Marshal(b, m, deterministic)
}
func (dst *GetStreamReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetStreamReply.Merge(dst, src)
}
func (m *GetStreamReply) XXX_Size() int {
	return xxx_messageInfo_GetStreamReply.Size(m)
}
func (m *GetStreamReply) XXX_DiscardUnknown() {
	xxx_messageInfo_GetStreamReply.DiscardUnknown(m)
}

var xxx_messageInfo_GetStreamReply proto.InternalMessageInfo

func (m *GetStreamReply) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *GetStreamReply) GetLive() bool {
	if m != nil {
		return m.Live
	}
	return false
}

type GetVersionMsg struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetVersionMsg) Reset()         { *m = GetVersionMsg{} }
func (m *GetVersionMsg) String() string { return proto.CompactTextString(m) }
func (*GetVersionMsg) ProtoMessage()    {}
func (*GetVersionMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_00212fb1f9d3bf1c, []int{2}
}
func (m *GetVersionMsg) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetVersionMsg.Unmarshal(m, b)
}
func (m *GetVersionMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetVersionMsg.Marshal(b, m, deterministic)
}
func (dst *GetVersionMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetVersionMsg.Merge(dst, src)
}
func (m *GetVersionMsg) XXX_Size() int {
	return xxx_messageInfo_GetVersionMsg.Size(m)
}
func (m *GetVersionMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_GetVersionMsg.DiscardUnknown(m)
}

var xxx_messageInfo_GetVersionMsg proto.InternalMessageInfo

type GetVersionReply struct {
	// If operation was OK
	Ok bool `protobuf:"varint,1,opt,name=ok,proto3" json:"ok,omitempty"`
	// Version Information
	VersionInformation   *GetVersionReply_VersionInformation `protobuf:"bytes,2,opt,name=version_information,json=versionInformation,proto3" json:"version_information,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                            `json:"-"`
	XXX_unrecognized     []byte                              `json:"-"`
	XXX_sizecache        int32                               `json:"-"`
}

func (m *GetVersionReply) Reset()         { *m = GetVersionReply{} }
func (m *GetVersionReply) String() string { return proto.CompactTextString(m) }
func (*GetVersionReply) ProtoMessage()    {}
func (*GetVersionReply) Descriptor() ([]byte, []int) {
	return fileDescriptor_00212fb1f9d3bf1c, []int{3}
}
func (m *GetVersionReply) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetVersionReply.Unmarshal(m, b)
}
func (m *GetVersionReply) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetVersionReply.Marshal(b, m, deterministic)
}
func (dst *GetVersionReply) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetVersionReply.Merge(dst, src)
}
func (m *GetVersionReply) XXX_Size() int {
	return xxx_messageInfo_GetVersionReply.Size(m)
}
func (m *GetVersionReply) XXX_DiscardUnknown() {
	xxx_messageInfo_GetVersionReply.DiscardUnknown(m)
}

var xxx_messageInfo_GetVersionReply proto.InternalMessageInfo

func (m *GetVersionReply) GetOk() bool {
	if m != nil {
		return m.Ok
	}
	return false
}

func (m *GetVersionReply) GetVersionInformation() *GetVersionReply_VersionInformation {
	if m != nil {
		return m.VersionInformation
	}
	return nil
}

type GetVersionReply_VersionInformation struct {
	// The tag on the git repository
	GitVersion string `protobuf:"bytes,1,opt,name=git_version,json=gitVersion,proto3" json:"git_version,omitempty"`
	// The hash of the git commit
	GitCommit string `protobuf:"bytes,2,opt,name=git_commit,json=gitCommit,proto3" json:"git_commit,omitempty"`
	// Whether or not the tree was clean when built
	GitTreeState string `protobuf:"bytes,3,opt,name=git_tree_state,json=gitTreeState,proto3" json:"git_tree_state,omitempty"`
	// Date of build
	BuildDate string `protobuf:"bytes,4,opt,name=build_date,json=buildDate,proto3" json:"build_date,omitempty"`
	// Version of go used to compile
	GoVersion string `protobuf:"bytes,5,opt,name=go_version,json=goVersion,proto3" json:"go_version,omitempty"`
	// Compiler used
	Compiler string `protobuf:"bytes,6,opt,name=compiler,proto3" json:"compiler,omitempty"`
	// Platform it was compiled for / running on
	Platform             string   `protobuf:"bytes,7,opt,name=platform,proto3" json:"platform,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetVersionReply_VersionInformation) Reset()         { *m = GetVersionReply_VersionInformation{} }
func (m *GetVersionReply_VersionInformation) String() string { return proto.CompactTextString(m) }
func (*GetVersionReply_VersionInformation) ProtoMessage()    {}
func (*GetVersionReply_VersionInformation) Descriptor() ([]byte, []int) {
	return fileDescriptor_00212fb1f9d3bf1c, []int{3, 0}
}
func (m *GetVersionReply_VersionInformation) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetVersionReply_VersionInformation.Unmarshal(m, b)
}
func (m *GetVersionReply_VersionInformation) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetVersionReply_VersionInformation.Marshal(b, m, deterministic)
}
func (dst *GetVersionReply_VersionInformation) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetVersionReply_VersionInformation.Merge(dst, src)
}
func (m *GetVersionReply_VersionInformation) XXX_Size() int {
	return xxx_messageInfo_GetVersionReply_VersionInformation.Size(m)
}
func (m *GetVersionReply_VersionInformation) XXX_DiscardUnknown() {
	xxx_messageInfo_GetVersionReply_VersionInformation.DiscardUnknown(m)
}

var xxx_messageInfo_GetVersionReply_VersionInformation proto.InternalMessageInfo

func (m *GetVersionReply_VersionInformation) GetGitVersion() string {
	if m != nil {
		return m.GitVersion
	}
	return ""
}

func (m *GetVersionReply_VersionInformation) GetGitCommit() string {
	if m != nil {
		return m.GitCommit
	}
	return ""
}

func (m *GetVersionReply_VersionInformation) GetGitTreeState() string {
	if m != nil {
		return m.GitTreeState
	}
	return ""
}

func (m *GetVersionReply_VersionInformation) GetBuildDate() string {
	if m != nil {
		return m.BuildDate
	}
	return ""
}

func (m *GetVersionReply_VersionInformation) GetGoVersion() string {
	if m != nil {
		return m.GoVersion
	}
	return ""
}

func (m *GetVersionReply_VersionInformation) GetCompiler() string {
	if m != nil {
		return m.Compiler
	}
	return ""
}

func (m *GetVersionReply_VersionInformation) GetPlatform() string {
	if m != nil {
		return m.Platform
	}
	return ""
}

func init() {
	proto.RegisterType((*GetStreamMsg)(nil), "twitchservice.GetStreamMsg")
	proto.RegisterType((*GetStreamReply)(nil), "twitchservice.GetStreamReply")
	proto.RegisterType((*GetVersionMsg)(nil), "twitchservice.GetVersionMsg")
	proto.RegisterType((*GetVersionReply)(nil), "twitchservice.GetVersionReply")
	proto.RegisterType((*GetVersionReply_VersionInformation)(nil), "twitchservice.GetVersionReply.VersionInformation")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// TwitchServiceAPIClient is the client API for TwitchServiceAPI service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type TwitchServiceAPIClient interface {
	// Will return version information about api server
	GetVersionInformation(ctx context.Context, in *GetVersionMsg, opts ...grpc.CallOption) (*GetVersionReply, error)
	// Will return stream information
	GetStreamInformation(ctx context.Context, in *GetStreamMsg, opts ...grpc.CallOption) (*GetStreamReply, error)
}

type twitchServiceAPIClient struct {
	cc *grpc.ClientConn
}

func NewTwitchServiceAPIClient(cc *grpc.ClientConn) TwitchServiceAPIClient {
	return &twitchServiceAPIClient{cc}
}

func (c *twitchServiceAPIClient) GetVersionInformation(ctx context.Context, in *GetVersionMsg, opts ...grpc.CallOption) (*GetVersionReply, error) {
	out := new(GetVersionReply)
	err := c.cc.Invoke(ctx, "/twitchservice.TwitchServiceAPI/GetVersionInformation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *twitchServiceAPIClient) GetStreamInformation(ctx context.Context, in *GetStreamMsg, opts ...grpc.CallOption) (*GetStreamReply, error) {
	out := new(GetStreamReply)
	err := c.cc.Invoke(ctx, "/twitchservice.TwitchServiceAPI/GetStreamInformation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// TwitchServiceAPIServer is the server API for TwitchServiceAPI service.
type TwitchServiceAPIServer interface {
	// Will return version information about api server
	GetVersionInformation(context.Context, *GetVersionMsg) (*GetVersionReply, error)
	// Will return stream information
	GetStreamInformation(context.Context, *GetStreamMsg) (*GetStreamReply, error)
}

func RegisterTwitchServiceAPIServer(s *grpc.Server, srv TwitchServiceAPIServer) {
	s.RegisterService(&_TwitchServiceAPI_serviceDesc, srv)
}

func _TwitchServiceAPI_GetVersionInformation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetVersionMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TwitchServiceAPIServer).GetVersionInformation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/twitchservice.TwitchServiceAPI/GetVersionInformation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TwitchServiceAPIServer).GetVersionInformation(ctx, req.(*GetVersionMsg))
	}
	return interceptor(ctx, in, info, handler)
}

func _TwitchServiceAPI_GetStreamInformation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetStreamMsg)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TwitchServiceAPIServer).GetStreamInformation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/twitchservice.TwitchServiceAPI/GetStreamInformation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TwitchServiceAPIServer).GetStreamInformation(ctx, req.(*GetStreamMsg))
	}
	return interceptor(ctx, in, info, handler)
}

var _TwitchServiceAPI_serviceDesc = grpc.ServiceDesc{
	ServiceName: "twitchservice.TwitchServiceAPI",
	HandlerType: (*TwitchServiceAPIServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetVersionInformation",
			Handler:    _TwitchServiceAPI_GetVersionInformation_Handler,
		},
		{
			MethodName: "GetStreamInformation",
			Handler:    _TwitchServiceAPI_GetStreamInformation_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api.proto",
}

func init() { proto.RegisterFile("api.proto", fileDescriptor_00212fb1f9d3bf1c) }

var fileDescriptor_00212fb1f9d3bf1c = []byte{
	// 574 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x7c, 0x53, 0xcf, 0x6f, 0xd3, 0x30,
	0x14, 0x56, 0xc2, 0x18, 0xad, 0xb7, 0xb5, 0xc3, 0xfc, 0xaa, 0xb2, 0x0d, 0xa2, 0x88, 0xc3, 0x34,
	0x68, 0xb2, 0x96, 0x0b, 0xda, 0x89, 0x02, 0xd2, 0x54, 0xa1, 0x4a, 0x53, 0x3a, 0xed, 0x1a, 0xb9,
	0xe9, 0xc3, 0xf5, 0x96, 0xc4, 0x91, 0xed, 0x66, 0x82, 0x23, 0x7f, 0x02, 0xfc, 0x69, 0x48, 0x1c,
	0x38, 0x73, 0x46, 0x1c, 0x39, 0xa2, 0xd8, 0x49, 0x5b, 0xa8, 0xc6, 0x29, 0x7e, 0xdf, 0xf7, 0xf9,
	0xbd, 0xf7, 0x3d, 0xbf, 0xa0, 0x26, 0xc9, 0x99, 0x9f, 0x0b, 0xae, 0x38, 0xde, 0x51, 0xd7, 0x4c,
	0xc5, 0x33, 0x09, 0xa2, 0x60, 0x31, 0x38, 0xfb, 0x94, 0x73, 0x9a, 0x40, 0x40, 0x72, 0x16, 0x90,
	0x2c, 0xe3, 0x8a, 0x28, 0xc6, 0x33, 0x69, 0xc4, 0xce, 0x73, 0xfd, 0x89, 0xbb, 0x14, 0xb2, 0xae,
	0xbc, 0x26, 0x94, 0x82, 0x08, 0x78, 0xae, 0x15, 0xeb, 0x6a, 0xef, 0x19, 0xda, 0x3e, 0x05, 0x35,
	0x56, 0x02, 0x48, 0x3a, 0x92, 0x14, 0xef, 0xa1, 0xa6, 0xd4, 0x41, 0xc4, 0xa6, 0x1d, 0xcb, 0xb5,
	0x0e, 0x9b, 0x61, 0xc3, 0x00, 0xc3, 0xa9, 0xf7, 0x12, 0xb5, 0x16, 0xe2, 0x10, 0xf2, 0xe4, 0x03,
	0xc6, 0x68, 0x23, 0x23, 0x29, 0x54, 0x4a, 0x7d, 0x2e, 0xb1, 0x84, 0x15, 0xd0, 0xb1, 0x5d, 0xeb,
	0xb0, 0x11, 0xea, 0xb3, 0xd7, 0x46, 0x3b, 0xa7, 0xa0, 0x2e, 0x40, 0x48, 0xc6, 0xb3, 0x91, 0xa4,
	0xde, 0x6f, 0x1b, 0xb5, 0x97, 0x88, 0x49, 0xd6, 0x42, 0x36, 0xbf, 0xd2, 0xa9, 0x1a, 0xa1, 0xcd,
	0xaf, 0xf0, 0x04, 0xdd, 0x2b, 0x0c, 0x1f, 0xb1, 0xec, 0x3d, 0x17, 0xa9, 0xee, 0x5c, 0xe7, 0xdd,
	0xea, 0xf7, 0xfc, 0xbf, 0x86, 0xe2, 0xff, 0x93, 0xcc, 0xaf, 0x82, 0xe1, 0xf2, 0x62, 0x88, 0x8b,
	0x35, 0xcc, 0xf9, 0x65, 0x21, 0xbc, 0x2e, 0xc5, 0x4f, 0xd0, 0x16, 0x65, 0x2a, 0xaa, 0x2e, 0x54,
	0xf6, 0x10, 0x65, 0x75, 0x0d, 0x7c, 0x80, 0xca, 0x28, 0x8a, 0x79, 0x9a, 0x32, 0xa5, 0x5b, 0x6a,
	0x86, 0x4d, 0xca, 0xd4, 0x1b, 0x0d, 0xe0, 0xa7, 0xa8, 0x55, 0xd2, 0x4a, 0x00, 0x44, 0x52, 0x11,
	0x05, 0x9d, 0x5b, 0x5a, 0xb2, 0x4d, 0x99, 0x3a, 0x17, 0x00, 0xe3, 0x12, 0x2b, 0x93, 0x4c, 0xe6,
	0x2c, 0x99, 0x46, 0xd3, 0x52, 0xb1, 0x61, 0x92, 0x68, 0xe4, 0x6d, 0x45, 0x53, 0xbe, 0xe8, 0xe1,
	0x76, 0x55, 0x83, 0xd7, 0x2d, 0x38, 0xa8, 0x11, 0xf3, 0x34, 0x67, 0x09, 0x88, 0xce, 0xa6, 0x79,
	0xa9, 0x3a, 0x2e, 0xb9, 0x3c, 0x21, 0xaa, 0x34, 0xd4, 0xb9, 0x63, 0xb8, 0x3a, 0xee, 0xff, 0xb4,
	0xd0, 0xee, 0xb9, 0x9e, 0xdd, 0xd8, 0xcc, 0x6e, 0x70, 0x36, 0xc4, 0x19, 0x7a, 0xb0, 0x9c, 0xe0,
	0xea, 0x24, 0xf6, 0x6f, 0x9c, 0xf3, 0x48, 0x52, 0xe7, 0xf1, 0xff, 0x5f, 0xc1, 0x7b, 0xf4, 0xe9,
	0xeb, 0x8f, 0x2f, 0xf6, 0x5d, 0xdc, 0xd6, 0xcb, 0x5a, 0xf4, 0x82, 0xca, 0x0d, 0xbe, 0x44, 0xf7,
	0x17, 0xab, 0xb4, 0x5a, 0x6e, 0x6f, 0x3d, 0xe1, 0x62, 0x39, 0x9d, 0x83, 0x9b, 0x48, 0x53, 0xec,
	0xa1, 0x2e, 0xb6, 0x8b, 0x5b, 0x75, 0x31, 0xb3, 0xb8, 0xaf, 0xbf, 0x5b, 0x9f, 0x07, 0xdf, 0x2c,
	0x1c, 0x21, 0x6c, 0x6c, 0xbb, 0x95, 0x6f, 0x77, 0x70, 0x36, 0xf4, 0x06, 0xa8, 0x3d, 0x62, 0xf1,
	0x8c, 0x40, 0xe2, 0x5e, 0x40, 0x06, 0x1f, 0x19, 0xc1, 0xce, 0x4c, 0xa9, 0x5c, 0x9e, 0x04, 0x01,
	0x65, 0x6a, 0x36, 0x9f, 0xf8, 0x31, 0x4f, 0x83, 0xc2, 0x70, 0x0e, 0x4e, 0xab, 0xd3, 0x2b, 0x9a,
	0x12, 0x96, 0x94, 0x5c, 0x7f, 0xb3, 0x38, 0xf6, 0x7b, 0xfe, 0xf1, 0x91, 0x6d, 0x5b, 0xfd, 0x5d,
	0x92, 0xe7, 0x09, 0x8b, 0xb5, 0x99, 0xe0, 0x52, 0xf2, 0xec, 0x64, 0x0d, 0x11, 0xef, 0xd0, 0xc1,
	0x88, 0x0b, 0x70, 0xc9, 0x84, 0xcf, 0x95, 0x6b, 0x2c, 0xb9, 0x72, 0xd9, 0x13, 0x3e, 0x5a, 0x69,
	0x20, 0x21, 0xa6, 0x81, 0xba, 0x6e, 0x60, 0xf4, 0xdd, 0x4a, 0x3f, 0xd9, 0xd4, 0xff, 0xf1, 0x8b,
	0x3f, 0x01, 0x00, 0x00, 0xff, 0xff, 0xac, 0xd0, 0x94, 0x7b, 0x2f, 0x04, 0x00, 0x00,
}
