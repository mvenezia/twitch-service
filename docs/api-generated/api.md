# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [api.proto](#api.proto)
    - [GetStreamMsg](#twitchservice.GetStreamMsg)
    - [GetStreamReply](#twitchservice.GetStreamReply)
    - [GetVersionMsg](#twitchservice.GetVersionMsg)
    - [GetVersionReply](#twitchservice.GetVersionReply)
    - [GetVersionReply.VersionInformation](#twitchservice.GetVersionReply.VersionInformation)
  
  
  
    - [TwitchServiceAPI](#twitchservice.TwitchServiceAPI)
  

- [api.proto](#api.proto)
    - [GetStreamMsg](#twitchservice.GetStreamMsg)
    - [GetStreamReply](#twitchservice.GetStreamReply)
    - [GetVersionMsg](#twitchservice.GetVersionMsg)
    - [GetVersionReply](#twitchservice.GetVersionReply)
    - [GetVersionReply.VersionInformation](#twitchservice.GetVersionReply.VersionInformation)
  
  
  
    - [TwitchServiceAPI](#twitchservice.TwitchServiceAPI)
  

- [Scalar Value Types](#scalar-value-types)



<a name="api.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="twitchservice.GetStreamMsg"></a>

### GetStreamMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| stream_id | [string](#string) |  | Stream id |






<a name="twitchservice.GetStreamReply"></a>

### GetStreamReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | What is the stream&#39;s name |
| live | [bool](#bool) |  | Is the stream live now? |






<a name="twitchservice.GetVersionMsg"></a>

### GetVersionMsg







<a name="twitchservice.GetVersionReply"></a>

### GetVersionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If operation was OK |
| version_information | [GetVersionReply.VersionInformation](#twitchservice.GetVersionReply.VersionInformation) |  | Version Information |






<a name="twitchservice.GetVersionReply.VersionInformation"></a>

### GetVersionReply.VersionInformation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| git_version | [string](#string) |  | The tag on the git repository |
| git_commit | [string](#string) |  | The hash of the git commit |
| git_tree_state | [string](#string) |  | Whether or not the tree was clean when built |
| build_date | [string](#string) |  | Date of build |
| go_version | [string](#string) |  | Version of go used to compile |
| compiler | [string](#string) |  | Compiler used |
| platform | [string](#string) |  | Platform it was compiled for / running on |





 

 

 


<a name="twitchservice.TwitchServiceAPI"></a>

### TwitchServiceAPI


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersionInformation | [GetVersionMsg](#twitchservice.GetVersionMsg) | [GetVersionReply](#twitchservice.GetVersionReply) | Will return version information about api server |
| GetStreamInformation | [GetStreamMsg](#twitchservice.GetStreamMsg) | [GetStreamReply](#twitchservice.GetStreamReply) | Will return stream information |

 



<a name="api.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## api.proto



<a name="twitchservice.GetStreamMsg"></a>

### GetStreamMsg



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| stream_id | [string](#string) |  | Stream id |






<a name="twitchservice.GetStreamReply"></a>

### GetStreamReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | What is the stream&#39;s name |
| live | [bool](#bool) |  | Is the stream live now? |






<a name="twitchservice.GetVersionMsg"></a>

### GetVersionMsg







<a name="twitchservice.GetVersionReply"></a>

### GetVersionReply



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ok | [bool](#bool) |  | If operation was OK |
| version_information | [GetVersionReply.VersionInformation](#twitchservice.GetVersionReply.VersionInformation) |  | Version Information |






<a name="twitchservice.GetVersionReply.VersionInformation"></a>

### GetVersionReply.VersionInformation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| git_version | [string](#string) |  | The tag on the git repository |
| git_commit | [string](#string) |  | The hash of the git commit |
| git_tree_state | [string](#string) |  | Whether or not the tree was clean when built |
| build_date | [string](#string) |  | Date of build |
| go_version | [string](#string) |  | Version of go used to compile |
| compiler | [string](#string) |  | Compiler used |
| platform | [string](#string) |  | Platform it was compiled for / running on |





 

 

 


<a name="twitchservice.TwitchServiceAPI"></a>

### TwitchServiceAPI


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersionInformation | [GetVersionMsg](#twitchservice.GetVersionMsg) | [GetVersionReply](#twitchservice.GetVersionReply) | Will return version information about api server |
| GetStreamInformation | [GetStreamMsg](#twitchservice.GetStreamMsg) | [GetStreamReply](#twitchservice.GetStreamReply) | Will return stream information |

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

